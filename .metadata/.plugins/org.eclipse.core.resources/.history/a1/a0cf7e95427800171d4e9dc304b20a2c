package com.capgemini.chess.dataaccess.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dataaccess.entities.UserEntity;
import com.capgemini.chess.dataaccess.entities.UserStatisticsEntity;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.mapper.UserProfileMapper;
import com.capgemini.chess.service.mapper.UserStatisticsMapper;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;

@Repository
public class MapUserDao implements UserDao {

	private static final Map<Long, UserEntity> users = new HashMap<>();

	public static Map<Long, UserEntity> getUsers() {
		return users;
	}

	@Override
	public UserProfileTO save(UserProfileTO userTo) {
		Long id = generateId();
		UserEntity userEntity = UserProfileMapper.map(userTo);
		//UserStatsTO userStatsTo = userTo.getStatistics();
		//if (userStatsTo == null) {
		UserStatsTO	userStatsTo = getDafaultStats(id);
		//}
		UserStatisticsEntity userStatisticsEntity = UserStatisticsMapper.map(userStatsTo);
		userEntity.setStatistics(userStatisticsEntity);
		userTo.setId(id);
		users.put(id, userEntity);
		return userTo;
	}

	private Long generateId() {
		return users.keySet().stream().max((i1, i2) -> i1.compareTo(i2)).orElse(0L) + 1;
	}

	@Override
	public UpdateProfileTO update(UpdateProfileTO to) {
		long id = to.getId();
		UserProfileTO userTo = findById(id);
		UserEntity userEntity = UserProfileMapper.map(userTo);
		userEntity = UserProfileMapper.update(userEntity, to);
		users.put(to.getId(), userEntity);
		return to;
	}

	@Override
	public UserStatsTO updateUserStats(UserStatsTO userStatsTo) {
		long id = userStatsTo.getId();
		UserProfileTO userTo = findById(id);
		UserStatisticsEntity userStatisticsEntity = UserStatisticsMapper.map(userStatsTo);
		UserEntity userEntity = UserProfileMapper.map(userTo);
		userEntity.setStatistics(userStatisticsEntity);
		users.put(userStatsTo.getId(), userEntity);
		return userStatsTo;
	}

	@Override
	public UserProfileTO findById(long id) {
		UserEntity user = users.get(id);
		return UserProfileMapper.map(user);
	}

	@Override
	public UserStatsTO findStatsById(long id) {
		UserStatisticsEntity user = users.get(id).getStatistics();
		return UserStatisticsMapper.map(user);
	}

	@Override
	public UserProfileTO findByEmail(String email) {
		UserEntity user = users.values().stream().filter(u -> u.getEmail().equals(email)).findFirst().orElse(null);
		return UserProfileMapper.map(user);
	}

	UserStatsTO getDafaultStats(long id) {
		UserStatsTO defaultStats = new UserStatsTO.Builder().id(id).gamesPlayed(0).won(0).lost(0).draw(0)
				.rankingPoints(100).level(Level.NEWBIE).build();
		return defaultStats;
	}

	@Override
	public List<UserStatsTO> readRanking() {
		List<UserStatisticsEntity> listOfUsersStatistics = new ArrayList<UserStatisticsEntity>();
		List<UserEntity> listOfUsers = new ArrayList<UserEntity>();
		listOfUsers.addAll(users.values());
		for (int i = 0; i < listOfUsers.size(); i++) {
			listOfUsersStatistics.add(listOfUsers.get(i).getStatistics());
		}
		Collections.sort(listOfUsersStatistics, new Comparator<UserStatisticsEntity>() {
			@Override
			public int compare(UserStatisticsEntity o1, UserStatisticsEntity o2) {
				return o2.getRankingPoints() - o1.getRankingPoints();
			}
		});
		return UserStatisticsMapper.mapStats2TOs(listOfUsersStatistics);
	}
}
