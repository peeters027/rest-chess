package com.capgemini.chess.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.facade.UserServiceFacade;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UserStatsTO;


@RunWith(MockitoJUnitRunner.class)
public class RestTests {

	@Mock
	private UserServiceFacade userServiceFacade;
	
	@InjectMocks
	private RestService restService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		Mockito.reset(userServiceFacade);
		this.mockMvc = MockMvcBuilders.standaloneSetup(restService).build();
	}
	
	@Test
	public void Test() throws Exception {

		// given:
		final RankingTO rankingTo1 = new RankingTO();
		rankingTo1.setRankingPlace(2);
		rankingTo1.setRanking(setUserStats());
		
		// register response for userServiceFacade.getRanking(1) mock
		Mockito.when(userServiceFacade.getRanking(1)).thenReturn(rankingTo1);
		
		// when
		ResultActions response = this.mockMvc.perform(get("/findStats/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content("1"));
		
		response.andExpect(status().isOk());
				//.andExpect(jsonPath("[0].ranking").value(rankingTo1.getRanking())
				//.andExpect(jsonPath("[0].rankingPlace").value(rankingTo1.getRankingPlace()));

	}

	public List<UserStatsTO> setUserStats()
	{
		
	List<UserStatsTO> userStats = new ArrayList<UserStatsTO>();
		
	UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(1000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(2000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(500)
			.level(Level.BEGINNER)
			.build();
	
	userStats.add(firstUserStats);
	userStats.add(secondUserStats);
	userStats.add(thirdUserStats);
	return userStats;

	}
	
	public List<UserStatsTO> setExpectedRanking()
	{
		
	List<UserStatsTO> userStats = new ArrayList<UserStatsTO>();
		
	UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(1000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(2000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(500)
			.level(Level.BEGINNER)
			.build();
	
	userStats.add(secondUserStats);
	userStats.add(firstUserStats);
	userStats.add(thirdUserStats);
	return userStats;

	}
}
