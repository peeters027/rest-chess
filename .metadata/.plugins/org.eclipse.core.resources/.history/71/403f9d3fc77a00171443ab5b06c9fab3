package com.capgemini.chess.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.chess.exception.UserValidationException;
import com.capgemini.chess.service.MatchRegisterService;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.access.UserDao;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;

@Controller
@ResponseBody
public class RestService {

	@Autowired
	private RankingService rankingService;

	@Autowired
	private UserUpdateService userUpdateService;

	@Autowired
	private MatchRegisterService matchRegisterService;

	@RequestMapping(value = "/findStats/{id}")
	public RankingTO findRankById(@PathVariable("id") long id) throws UserValidationException {
		return rankingService.getRanking(id);
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public UserProfileTO updateProfile(@RequestBody UpdateProfileTO updateProfileTo) throws UserValidationException {
		return userUpdateService.updateProfile(updateProfileTo);
	}

	@RequestMapping(value = "/registerNewMatch", method = RequestMethod.POST)
	public MatchTO registerNewMatch(@RequestBody MatchTO matchTo) throws UserValidationException {
		return matchRegisterService.registerNewMatch(matchTo);
	}
}
