package com.capgemini.chess.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.capgemini.chess.enums.Level;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserUpdateService;
import com.capgemini.chess.service.to.RankingTO;
import com.capgemini.chess.service.to.UpdateProfileTO;
import com.capgemini.chess.service.to.UserProfileTO;
import com.capgemini.chess.service.to.UserStatsTO;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(MockitoJUnitRunner.class)
public class RestTests {

	@Mock
	private RankingService rankingService;
	
	@Mock
	private UserUpdateService userUpdateService;
	
	@InjectMocks
	private RestService restService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		Mockito.reset(rankingService);
		this.mockMvc = MockMvcBuilders.standaloneSetup(restService).build();
	}
	
	@Test
	public void shouldGetUserRanking() throws Exception{

		// given:
		final RankingTO rankingTo1 = new RankingTO();
		rankingTo1.setRankingPlace(2);
		rankingTo1.setRanking(setRanking());
		
		// register response for userServiceFacade.getRanking(1) mock
		Mockito.when(rankingService.getRanking(1)).thenReturn(rankingTo1);
		
		// when
		ResultActions response = this.mockMvc.perform(get("/findStats/1"));
		
		response.andExpect(status().isOk())
				.andExpect(jsonPath("ranking[0].id").value((int)rankingTo1.getRanking().get(0).getId()))
				.andExpect(jsonPath("rankingPlace").value(rankingTo1.getRankingPlace()));
				//.andExpect(jsonPath("[0].rankingPlace").value(rankingTo1.getRankingPlace()));
	}
	
	@Test
	public void shouldUpdateUserProfile() throws Exception {

		// given:
		UpdateProfileTO updateUser = setUpdateProfile();
		UserProfileTO userAfterUpdate = setExpectedProfileAfterUpdate();
		String jsonUpdateUser = new ObjectMapper().writeValueAsString(updateUser); 
		
		// register response for userServiceFacade.getRanking(1) mock
		Mockito.when(userUpdateService.updateProfile(updateUser)).thenReturn(userAfterUpdate);

		// when
		ResultActions response = this.mockMvc.perform(post("/updateUser").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(jsonUpdateUser));

		response.andExpect(status().isOk())
				.andExpect(jsonPath("id").value(userAfterUpdate.getId()));
				//.andExpect(jsonPath("rankingPlace").value(rankingTo1.getRankingPlace()));
		// .andExpect(jsonPath("[0].rankingPlace").value(rankingTo1.getRankingPlace()));
	}
	
	public void brudnopis() throws Exception{
		// path variable:
		ResultActions response = this.mockMvc.perform(get("/findStats/1"));
		// request parameters dla URL localhost:8092/findStats/?id=1
		ResultActions response1 = this.mockMvc.perform(get("/findStats/").param("id", "1"));
		// Json (POST)
		ResultActions response2 = this.mockMvc.perform(post("/findStats/").content("heres my json content"));
	}

	public List<UserStatsTO> setRanking()
	{
		
	List<UserStatsTO> userStats = new ArrayList<UserStatsTO>();
		
	UserStatsTO firstUserStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(1000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO secondUserStats = new UserStatsTO.Builder().id(2).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(2000)
			.level(Level.BEGINNER)
			.build();
	
	UserStatsTO thirdUserStats = new UserStatsTO.Builder().id(3).gamesPlayed(100)
			.won(50)
			.lost(50)
			.draw(0)
			.rankingPoints(500)
			.level(Level.BEGINNER)
			.build();
	
	userStats.add(secondUserStats);
	userStats.add(firstUserStats);
	userStats.add(thirdUserStats);
	return userStats;

	}
	
	public UpdateProfileTO setUpdateProfile()
	{	
	UpdateProfileTO updateProfile = new UpdateProfileTO.Builder().id(1)
			.name("Tomek")
			.surname("Szmolke")
			.email("piotr@gmail.com")
			.aboutMe("Happy")
			.lifeMotto("Dopóki walczysz jesteś zwyciezcą")
			.build();

	return updateProfile;
	}
	
	public UserProfileTO setExpectedProfileAfterUpdate()
	{	
		UserStatsTO userStats = new UserStatsTO.Builder().id(1).gamesPlayed(100)
				.won(50)
				.lost(50)
				.draw(0)
				.rankingPoints(1000)
				.level(Level.BEGINNER)
				.build();
		
		UserProfileTO expectedUserAfterUpdated = new UserProfileTO.Builder().id(1)
				.login("Lion")
				.name("Tomek")
				.surname("Szmolke")
				.email("piotr@gmail.com")
				.aboutMe("Happy")
				.lifeMotto("Dopóki walczysz jesteś zwyciezcą")
				.statistics(userStats)
				.build();
		
		return expectedUserAfterUpdated;
	}	
}
